package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// TODO: Import service bean
@Service
public class SoulServiceImpl implements SoulService {
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service
    @Autowired
    private SoulRepository soulRepository;

    @Override
    public List<Soul> findAll() {
        return soulRepository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return soulRepository.findById(id);
    }

    @Override
    public void erase(Long id) {
        soulRepository.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        soulRepository.save(soul);
        return soul;
    }

    @Override
    public Soul register(Soul soul) {
        soulRepository.save(soul);
        return soul;
    }

}

