package id.ac.ui.cs.advprog.tutorial5.model;

public class SoulBuilder {
    private String name;
    private int age;
    private String gender;
    private String occupation;

    public SoulBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SoulBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public SoulBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public SoulBuilder setOccupation(String occupation) {
        this.occupation = occupation;
        return this;
    }

    public Soul createSoul() {
        return new Soul(name, age, gender, occupation);
    }
}
