package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @Test
    public void whenProduceKnightIsCalledItShouldProduceAKnight() {
        String academyName, knightType;

        academyName = "lordran";
        knightType = "majestic";

        KnightAcademy lordran = new LordranAcademy();

        when(academyRepository.getKnightAcademyByName(academyName))
                .thenReturn(lordran);

        academyService.produceKnight(academyName, knightType);

        Knight knight = academyService.getKnight();

        assertThat(knight.getName()).isEqualTo("MajesticKnight");
    }

    @Test
    public void whenGetKnightIsCalledItShouldReturnKnight() {
        String academyName, knightType;

        academyName = "lordran";
        knightType = "majestic";

        KnightAcademy lordran = new LordranAcademy();

        when(academyRepository.getKnightAcademyByName(academyName))
                .thenReturn(lordran);

        academyService.produceKnight(academyName, knightType);

        Knight knight = academyService.getKnight();

        assertThat(knight.getClass().getSimpleName()).isEqualTo("MajesticKnight");
    }

    @Test
    public void whenGetKnightAcademiesIsCalledItShouldReturnListOfKnightAcademy() {
        List<KnightAcademy> knightList = new ArrayList<>();
        when(academyService.getKnightAcademies()).thenReturn(knightList);
        List<KnightAcademy> lst= academyService.getKnightAcademies();
        assertThat(knightList).isEqualTo(lst);
    }

    @Test
    public void testRepository() {
        AcademyRepository repository = new AcademyRepository();
        List<KnightAcademy> knightList = new ArrayList<>();
        List<KnightAcademy> lst= repository.getKnightAcademies();
        assertThat(lst).isEqualTo(knightList);

        KnightAcademy knightAcademy = new DrangleicAcademy();
        repository.addKnightAcademy("Drangleic", knightAcademy);
        knightList.add(knightAcademy);
        lst= repository.getKnightAcademies();
        assertThat(lst).isEqualTo(knightList);

        KnightAcademy getKnightAcademy = repository.getKnightAcademyByName("Drangleic");
        assertThat(getKnightAcademy).isEqualTo(knightAcademy);
    }
}
